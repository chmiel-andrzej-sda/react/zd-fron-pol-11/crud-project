export function Requestor(): JSX.Element {
	function handleRead(): void {
		fetch('https://reqres.in/api/users', {
			method: 'GET'
		})
		.then(response => response.json())
		.then(response => response.data)
		.then(console.log);
	}

	function handleCreate(): void {
		fetch('https://reqres.in/api/users', {
			method: 'POST',
			body: JSON.stringify({name: 'Andrzej', job: 'Programming Trainer'})
		})
		.then(response => response.json())
		.then(console.log);
	}
	
	return <div>
		<button onClick={handleRead}>POBIERZ</button>
		<button onClick={handleCreate}>WYŚLIJ</button>
	</div>;
}

// 1. component for user list
// 2. useEffect -> call the api and download all users
// 3. save users to state (create also a user entity)
// 4. Render users from state