import React from "react";
import { User } from "./User";

export function UsersList(): JSX.Element {
	const [users, setUsers] = React.useState<User[]>([]);
	const [firstName, setFirstName] = React.useState<string>('');
	const [lastName, setLastName] = React.useState<string>('');
	const [editing, setEditing] = React.useState<number | undefined>(undefined);
	const [editFirstName, setEditFirstName] = React.useState<string>('');
	const [editLastName, setEditLastName] = React.useState<string>('');

	React.useEffect(() => {
		fetch('https://reqres.in/api/users', {
			method: 'GET'
		})
		.then(response => response.json())
		.then(response => response.data)
		.then(response => setUsers(response));
	}, []);

	function deleteUser(id: number): void {
		fetch(`https://reqres.in/api/users/${id}`, {
			method: 'DELETE'
		});
		setUsers(users.filter((u: User): boolean => u.id !== id));
	}

	function renderUser(user: User): JSX.Element {
		if (user.id === editing) {
			return <div key={user.id}>
				<input onChange={handleEditFirstNameChange} value={editFirstName}/>
				<input onChange={handleEditLastNameChange} value={editLastName}/>
				<button onClick={editUser}>EDIT</button>
			</div>;
		}

		return <p key={user.id}>
			<button onClick={() => setEditUser(user.id)}>EDIT</button>
			{`${user.first_name} ${user.last_name}`} <button onClick={() => deleteUser(user.id)}>X</button>
		</p>;
	}

	function setEditUser(id: number): void {
		setEditing(id);
		const index: number = users.findIndex((u: User): boolean => u.id === id);
		setEditFirstName(users[index].first_name);
		setEditLastName(users[index].last_name);
	}

	function editUser(): void {
		fetch(`https://reqres.in/api/users/${editing}`, {
			method: 'PATCH',
			body: JSON.stringify({first_name: editFirstName, last_name: editLastName})
		});
		if (editing !== undefined) {
			const index: number = users.findIndex((u: User): boolean => u.id === editing);
			users[index] = {
				...users[index],
				first_name: editFirstName,
				last_name: editLastName
			}
			setEditing(undefined);
		}
	}

	function addUser(): void {
		fetch(`https://reqres.in/api/users`, {
			method: 'POST',
			body: JSON.stringify({first_name: firstName, last_name: lastName})
		})
			.then(response => response.json())
			.then(response => {
				setUsers([...users, {
					first_name: firstName, 
					last_name: lastName, 
					avatar: '', 
					email: '', 
					id: response.id
				}]);
			});
		setFirstName('');
		setLastName('');
	}

	function handleEditFirstNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setEditFirstName(event.target.value);
	}

	function handleEditLastNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setEditLastName(event.target.value);
	}

	function handleFirstNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setFirstName(event.target.value);
	}

	function handleLastNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setLastName(event.target.value);
	}

	return <div>
		{users.map(renderUser)}
		<input onChange={handleFirstNameChange} value={firstName}/>
		<input onChange={handleLastNameChange} value={lastName}/>
		<button onClick={addUser}>ADD</button>
	</div>;
}

// 4. accept button sends the PUT and hides inputs (remove the id from state)