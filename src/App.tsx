import { Requestor } from "./Requestor";
import { UsersList } from "./UsersList";

function App() {
	return <div className="App">
		<Requestor/>
		<UsersList/>
	</div>;
}

export default App;
